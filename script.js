const statusToClass = {
    created: 'text-primary',
    executing: 'text-secondary',
    completed: 'text-warning',
    closed: 'text-success',
    rejected: 'text-danger'
}

async function loadWeb3() {
    if (window.ethereum) {
        window.web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'))
        window.ethereum.enable()
    }
    
}

async function load() {
    await loadWeb3()
    window.contract = await loadContract()
    console.log('Ready!!!')
}

async function createTask () {
    const name = document.getElementById('taskName').value
    const description = document.getElementById('taskDescription').value
    const reward = document.getElementById('taskReward').value
    const rewardWei = web3.utils.toWei(reward, 'ether')

    console.log(`creating task ${name} - ${description} - ${reward} from ${(await window.web3.eth.personal.getAccounts())[0]}`)

    await window.contract.methods.createTask(name, description).send({from: (await window.web3.eth.personal.getAccounts())[0], value: rewardWei, gas:3000000})
    alert("Task created successfully!")
    location.reload()
}

async function getAllTasks () {
    console.log(window.contract.methods)
    const taskHashes = (await window.contract.methods.getTaskHashes().call())
    const createdTasksHashes = await window.contract.methods.getUserCreatedTaskHashes().call()
    

    console.log(taskHashes)
    const tasks = []
    const reversedHashes = [...taskHashes].reverse()

    if (reversedHashes.length === 0) {
        document.getElementById("allTasksContainer").insertAdjacentHTML(
            'beforeEnd',
            `
            <div class="row text-center mt-4 mb-3 ">
                <h3>No tasks yet. Create the first one!</h3>
            </div>
            `
            )
    }

    for (const hash of reversedHashes) {
        const task = await window.contract.methods.hashToTask(hash).call()
        tasks.push(task)
        if (createdTasksHashes.includes(hash)) {
            document.getElementById("allTasksContainer").insertAdjacentHTML(
                'beforeEnd',
                `
                <div class="row text-left mt-4 mb-3 ">
                    <div class="col-12 card-body card">
                        <h3 class="card-title"><span class="${statusToClass[task.status]}" id="status_${task.hash}">${task.status}</span> | ${task.name} | ${task.reward / 10 ** 18} ETH </h3>
                        <h4 class="card-title text-success">Created by you</h4>
                        <p class="card-text">${task.description}</p>
                    </div>
                </div>
                `
                )
        } else {
            document.getElementById("allTasksContainer").insertAdjacentHTML(
                'beforeEnd',
                `
                <div class="row text-left mt-4 mb-3 ">
                    <div class="col-12 card-body card">
                        <h3 class="card-title"><span class="${statusToClass[task.status]}" id="status_${task.hash}">${task.status}</span> | ${task.name} </h3>
                        <p class="card-text">${task.description}</p>
                        <button class="btn btn-block btn-primary" ${task.status !== 'created' ? 'disabled' : ''} id="btn_${task.hash}" onclick="takeUpTask('${task.hash}')">Take up</button>
                    </div>
                </div>
                `
                )
        }
    }

    console.log(tasks)
}

async function getUserTasks () {
    const createdTasksHashes = await window.contract.methods.getUserCreatedTaskHashes().call()
    const createdTasks = []

    const executedTasksHashes = await window.contract.methods.getUserExecutedTaskHashes().call()
    const executedTasks = []

    if (createdTasksHashes.length === 0) {
        document.getElementById("createdTasksContainer").insertAdjacentHTML(
            'beforeEnd',
            `
            <div class="row text-center mt-4 mb-3 ">
                <h3>No tasks yet. Create the first one!</h3>
            </div>
            `
            )
    }

    if (executedTasksHashes.length === 0) {
        document.getElementById("executingTasksContainer").insertAdjacentHTML(
            'beforeEnd',
            `
            <div class="col-12 text-center mt-4 mb-3">
                <h3>You don't execute tasks. Select a task to execute in browse tab!</h3>
            </div>
            `
            )
    }

    for (const createdHash of createdTasksHashes) {
        const task = await window.contract.methods.hashToTask(createdHash).call()
        createdTasks.push(task)
        console.log(task)
        document.getElementById("createdTasksContainer").insertAdjacentHTML(
            'beforeEnd',
            `
            <div class="row text-left mt-4 mb-3 ">
                <div class=" card-body card">
                    <h3 class="card-title"><span class="${statusToClass[task.status]}" id="status_${task.hash}">${task.status}</span> | ${task.name} | ${task.reward / 10 ** 18} ETH </h3>
                    <p class="card-text">${task.description}</p>
                    ${task.status === 'completed' ? `
                        <h4 class="card-text"><b>Result:</b> ${task.result}</h4>
                        <div class="btn-group" id="controls_${task.hash}" role="group">
                            <button onclick=closeTask('${task.hash}') class="btn btn-success">Approve</button>
                            <button onclick=rejectTask('${task.hash}') class="btn btn-danger">Reject</button>
                        </div>
                    ` : ''}
                </div>
            </div>
            `
        )
    }

    for (const executedHash of executedTasksHashes) {
        const task = await window.contract.methods.hashToTask(executedHash).call()
        executedTasks.push(task)
        document.getElementById("executingTasksContainer").insertAdjacentHTML(
            'beforeEnd',
            `
            <div class="row text-left mt-4 mb-3 ">
                <div class="col-8 card-body card">
                    <h3 class="card-title"><span class="${statusToClass[task.status]}" id="status_${task.hash}">${task.status}</span> | ${task.name} | ${task.reward / 10 ** 18} ETH </h3>
                    <p class="card-text">${task.description}</p>
                    ${task.status === 'executing' ? `
                        <input type="text" placeholder="result of your work" id="result_${task.hash}" class="form-control mb-2 mt-4"></input>
                        <button onclick=completeTask('${task.hash}') class="btn btn-primary" id="btn_${task.hash}">Submit task</button>
                    ` : ''}
                </div>
            </div>
            `
        )
    }

    console.log(createdTasks)
    console.log(executedTasks)
}

async function takeUpTask (hash) {
    console.log(await window.web3.eth.personal.getAccounts())
    console.log(`take up task ${hash}`)
    try {
        await window.contract.methods.takeUpTask(hash).send({from: (await window.web3.eth.personal.getAccounts())[0], gas:3000000})
        document.getElementById(`status_${hash}`).className = 'text-secondary'
        document.getElementById(`status_${hash}`).innerHTML = 'executing'
        document.getElementById(`btn_${hash}`).disabled = true
    } catch (e) {
        console.log(e)
        alert('you cant take your own tasks!')
    }

    console.log('done')
}

async function completeTask (hash) {
    const result = document.getElementById(`result_${hash}`).value

    console.log(`submit task ${hash} ${result}`)
    await window.contract.methods.completeTask(hash, result).send({from: (await window.web3.eth.personal.getAccounts())[0], gas:3000000})
    document.getElementById(`status_${hash}`).className = 'text-warning'
    document.getElementById(`status_${hash}`).innerHTML = 'completed'
    document.getElementById(`btn_${hash}`).style.display = 'none'
    document.getElementById(`result_${hash}`).style.display = 'none'
}

async function closeTask (hash) {
    console.log(`close task ${hash}`)
    await window.contract.methods.closeTask(hash).send({from: (await window.web3.eth.personal.getAccounts())[0], gas:3000000})
    document.getElementById(`status_${hash}`).className = 'text-success'
    document.getElementById(`status_${hash}`).innerHTML = 'closed'
    document.getElementById(`controls_${hash}`).style.display = 'none'
}

async function rejectTask(hash) {
    console.log(`reject task ${hash}`)
    await window.contract.methods.rejectTask(hash).send({from: (await window.web3.eth.personal.getAccounts())[0], gas:3000000})
    document.getElementById(`status_${hash}`).className = 'text-danger'
    document.getElementById(`status_${hash}`).innerHTML = 'rejected'
    document.getElementById(`controls_${hash}`).style.display = 'none'
}

async function loadContract() {
    const ABI = [
        {
            "constant": true,
            "inputs": [],
            "name": "getTaskHashes",
            "outputs": [
                {
                    "name": "",
                    "type": "bytes32[]"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "_name",
                    "type": "string"
                },
                {
                    "name": "_description",
                    "type": "string"
                }
            ],
            "name": "createTask",
            "outputs": [],
            "payable": true,
            "stateMutability": "payable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "hash",
                    "type": "bytes32"
                }
            ],
            "name": "takeUpTask",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "getUserExecutedTaskHashes",
            "outputs": [
                {
                    "name": "",
                    "type": "bytes32[]"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [
                {
                    "name": "",
                    "type": "bytes32"
                }
            ],
            "name": "hashToTask",
            "outputs": [
                {
                    "name": "hash",
                    "type": "bytes32"
                },
                {
                    "name": "name",
                    "type": "string"
                },
                {
                    "name": "description",
                    "type": "string"
                },
                {
                    "name": "status",
                    "type": "string"
                },
                {
                    "name": "result",
                    "type": "string"
                },
                {
                    "name": "reward",
                    "type": "uint256"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "hash",
                    "type": "bytes32"
                },
                {
                    "name": "_result",
                    "type": "string"
                }
            ],
            "name": "completeTask",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": true,
            "inputs": [],
            "name": "getUserCreatedTaskHashes",
            "outputs": [
                {
                    "name": "",
                    "type": "bytes32[]"
                }
            ],
            "payable": false,
            "stateMutability": "view",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "hash",
                    "type": "bytes32"
                }
            ],
            "name": "rejectTask",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        },
        {
            "constant": false,
            "inputs": [
                {
                    "name": "hash",
                    "type": "bytes32"
                }
            ],
            "name": "closeTask",
            "outputs": [],
            "payable": false,
            "stateMutability": "nonpayable",
            "type": "function"
        }
    ]
    return await new window.web3.eth.Contract(
        ABI, 
        '0x34C6FFc4635B4Ca7211F5Ea52447c6fdd02b977E'
    )
}

load();