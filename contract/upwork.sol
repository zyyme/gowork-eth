// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.4.16 <0.9.0;
pragma experimental ABIEncoderV2;

contract Upwork {
    struct Task {
        bytes32 hash;
        string name;
        string description;
        string status;
        string result;
        uint256 reward;
    }

    bytes32[] tasksHashes;
    mapping (address => bytes32[]) creatorToTask;
    mapping (address => bytes32[]) executorToTask;
    mapping (bytes32 => address) hashToExecutor;
    mapping (bytes32 => Task) public hashToTask;

    function createTask (string memory _name, string memory _description) public payable {
        require(msg.value > 0, "provide sufficient reward");
        Task memory task = Task(
            keccak256(abi.encodePacked(_name, _description, msg.value)),    
            _name, 
            _description, 
            "created", 
            "",
            msg.value
        );
        creatorToTask[msg.sender].push(task.hash);
        tasksHashes.push(task.hash);
        hashToTask[task.hash] = task;
    }

    function getUserCreatedTaskHashes () public view returns (bytes32[] memory) {
        return creatorToTask[msg.sender];
    }

    function getUserExecutedTaskHashes () public view returns (bytes32[] memory) {
        return executorToTask[msg.sender];
    }

    function getTaskHashes () public view returns (bytes32[] memory) {
        return tasksHashes;
    }

    function takeUpTask (bytes32 hash) public {
        bytes32[] userTasks = creatorToTask[msg.sender];

        for (uint i = 0; i < userTasks.length; i ++) {
            require(userTasks[i] != hash, "You can't take your own task");
        }
        hashToTask[hash].status = "executing";
        executorToTask[msg.sender].push(hash);
        hashToExecutor[hash] = msg.sender;
    }

    function completeTask (bytes32 hash, string memory _result) public {
        bytes32[] userTasks = executorToTask[msg.sender];

        for (uint i = 0; i < userTasks.length; i ++) {
            if (userTasks[i] == hash) {
                hashToTask[hash].result = _result;
                hashToTask[hash].status = "completed";
                return;
            }
        }        
    }

    function closeTask (bytes32 hash) public {
        bytes32[] userTasks = creatorToTask[msg.sender];

        for (uint i = 0; i < userTasks.length; i ++) {
            if (userTasks[i] == hash) {
                hashToTask[hash].status = "closed";
                hashToExecutor[hash].transfer(hashToTask[hash].reward);
                return;
            }
        }
    }

    function rejectTask (bytes32 hash) public {
        bytes32[] userTasks = creatorToTask[msg.sender];

        for (uint i = 0; i < userTasks.length; i ++) {
            if (userTasks[i] == hash) {
                hashToTask[hash].status = "rejected";
                msg.sender.transfer(hashToTask[hash].reward);
                return;
            }
        }
    }
}